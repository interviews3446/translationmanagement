﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Moq;
using TranslationManagement.Data;
using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Entities.TranslatorManagement;
using TranslationManagement.Data.Repositories;

namespace TranslationManagement.Tests.TranslationManagements
{
    public class TranslationManagementRepositoryTests
    {
        private readonly Mock<AppDbContext> _mockContext;
        private readonly TranslationManagementRepository _translationManagementRepository;
        private readonly Mock<DbSet<TranslatorModel>> _mockTranslatorModelSet;

        public TranslationManagementRepositoryTests()
        {
            _mockContext = new Mock<AppDbContext>();
            _mockTranslatorModelSet = new Mock<DbSet<TranslatorModel>>();
            
            var translators = new[]
            {
                new TranslatorModel
                {
                    Id = 1, Name = "Translator 1", HourlyRate = "10", Status = TranslatorStatus.Applicant
                    , CreditCardNumber = "1234567890123456"
                }
                , new TranslatorModel()
                {
                    Id = 2, Name = "Translator 2", HourlyRate = "20", Status = TranslatorStatus.Applicant
                    , CreditCardNumber = "1234567890123456"
                }
            }.AsQueryable();

            _mockTranslatorModelSet.As<IQueryable<TranslatorModel>>().Setup(m => m.Provider).Returns(translators.Provider);
            _mockTranslatorModelSet.As<IQueryable<TranslatorModel>>().Setup(m => m.Expression).Returns(translators.Expression);
            _mockTranslatorModelSet.As<IQueryable<TranslatorModel>>().Setup(m => m.ElementType).Returns(translators.ElementType);
            _mockTranslatorModelSet.As<IEnumerable<TranslatorModel>>().Setup(m => m.GetEnumerator()).Returns(translators.GetEnumerator());



            _mockContext.Setup(c => c.Translators).Returns(_mockTranslatorModelSet.Object);

            _translationManagementRepository = new TranslationManagementRepository(_mockContext.Object);
        }

        [Fact]
        public void GetTranslators_ReturnsQueryable()
        {
            var result = _translationManagementRepository.GetTranslators().ToList();

            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async Task CreateTranslatorAsync_AddsTranslatorToContext()
        {
            var translator = new TranslatorModel();
            var mockSet = new Mock<DbSet<TranslatorModel>>();

            _mockContext.Setup(m => m.Translators).Returns(mockSet.Object);
            mockSet.Setup(m => m.AddAsync(It.IsAny<TranslatorModel>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync((TranslatorModel translator, CancellationToken token) => null!);

            await _translationManagementRepository.CreateTranslatorAsync(translator, CancellationToken.None);

            mockSet.Verify(m => m.AddAsync(translator, It.IsAny<CancellationToken>()), Times.Once);
            _mockContext.Verify(m => m.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }


        [Fact]
        public async Task SaveChangesAsync_SavesChangesInContext()
        {
            await _translationManagementRepository.SaveChangesAsync(CancellationToken.None);

            _mockContext.Verify(m => m.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
