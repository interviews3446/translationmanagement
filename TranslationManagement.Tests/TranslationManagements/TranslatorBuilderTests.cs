﻿using TranslationManagement.Data.Builders;
using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Tests.TranslationManagements
{
    public class TranslatorBuilderTests
    {
        private readonly TranslatorModelBuilder _builder;

        public TranslatorBuilderTests()
        {
            _builder = new TranslatorModelBuilder();
        }

        [Fact]
        public void WithName_SetsNameCorrectly()
        {
            var name = "Name";
            var model = _builder.WithName(name).Build();

            Assert.Equal(name, model.Name);
        }

        [Fact]
        public void WithHourlyRate_SetsHourlyRateCorrectly()
        {
            var hourlyRate = "100";
            var model = _builder.WithHourlyRate(hourlyRate).Build();

            Assert.Equal(hourlyRate, model.HourlyRate);
        }

        [Fact]
        public void WithStatus_SetsStatusCorrectly()
        {
            var status = TranslatorStatus.Applicant;
            var model = _builder.WithStatus(status).Build();

            Assert.Equal(status, model.Status);
        }

        [Fact]
        public void WithCreditCardNumber_SetsCreditCardNumberCorrectly()
        {
            var creditCardNumber = "1234567890123456";
            var model = _builder.WithCreditCardNumber(creditCardNumber).Build();

            Assert.Equal(creditCardNumber, model.CreditCardNumber);
        }

        [Fact]
        public void Build_ResetsInstanceAfterBuilding()
        {
            var model1 = _builder.WithName("John").Build();
            var model2 = _builder.Build();

            Assert.NotEqual(model1.Name, model2.Name);
            Assert.Null(model2.Name);
        }
    }
}