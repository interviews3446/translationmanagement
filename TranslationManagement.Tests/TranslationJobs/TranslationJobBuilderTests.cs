﻿using Microsoft.Extensions.Options;
using Moq;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Builders;
using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Tests.TranslationJobs
{
    public class TranslationJobBuilderTests
    {
        private readonly Mock<IOptions<TranslationManagementOptions>> _mockOptions;
        private readonly TranslationJobBuilder _builder;
        public TranslationJobBuilderTests()
        {
            var options = new TranslationManagementOptions
            {
                PricePerCharacter = 0.01m
            };

            _mockOptions = new Mock<IOptions<TranslationManagementOptions>>();
            _mockOptions.Setup(o => o.Value).Returns(options);

            _builder = new TranslationJobBuilder(_mockOptions.Object);
        }

        [Fact]
        public void WithCustomerName_SetsCustomerName()
        {
            var name = "Test Customer";

            var job = _builder.WithCustomerName(name).Build();

            Assert.Equal(name, job.CustomerName);
        }

        [Fact]
        public void WithStatus_SetsStatus()
        {
            var status = JobStatus.Completed;

            var job = _builder.WithStatus(status).Build();

            Assert.Equal(status, job.Status);
        }

        [Fact]
        public void WithOriginalContent_SetsOriginalContent()
        {
            var content = "Original content";

            var job = _builder.WithOriginalContent(content).Build();

            Assert.Equal(content, job.OriginalContent);
        }

        [Fact]
        public void ComputePrice_SetsCorrectPrice()
        {
            var content = "12345";

            var job = _builder.WithOriginalContent(content).ComputePrice().Build();

            Assert.Equal(content.Length * _mockOptions.Object.Value.PricePerCharacter, job.Price);
        }

        [Fact]
        public void Build_ResetsBuilderForSubsequentUse()
        {
            var firstJob = _builder.WithCustomerName("First Customer").Build();
            var secondJob = _builder.Build();

            Assert.Null(secondJob.CustomerName);
            Assert.NotEqual(firstJob.CustomerName, secondJob.CustomerName);
        }
    }

}
