﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using TranslationManagement.Common.Exceptions;
using TranslationManagement.Common.Options;
using TranslationManagement.Data;
using TranslationManagement.Data.Builders;
using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Entities.TranslationJob;
using TranslationManagement.Data.Repositories;

namespace TranslationManagement.Tests.TranslationJobs
{
    public class TranslationJobRepositoryTests
    {
        private readonly TranslationJobRepository _translationJobRepository;
        private readonly Mock<AppDbContext> _mockContext;
        private readonly Mock<DbSet<TranslationJob>> _mockTranslatonJobSet;
        private readonly List<TranslationJob> _translationJobs;
        private readonly TranslationJobBuilder _builder;
        private readonly Mock<IOptions<TranslationManagementOptions>> _mockOptions;

        public TranslationJobRepositoryTests()
        {
            var options = new TranslationManagementOptions
            {
                PricePerCharacter = (decimal)0.01
            };

            _mockOptions = new Mock<IOptions<TranslationManagementOptions>>();
            _mockOptions.Setup(o => o.Value).Returns(options);

            _mockContext = new Mock<AppDbContext>();
            _mockTranslatonJobSet = new Mock<DbSet<TranslationJob>>();
            _builder = new TranslationJobBuilder(_mockOptions.Object);

            _translationJobs = new List<TranslationJob>
            {
                _builder.WithCustomerName("Customer 1").WithStatus(JobStatus.New).WithOriginalContent("Original content 1").ComputePrice().Build(),
                _builder.WithCustomerName("Customer 2").WithStatus(JobStatus.New).WithOriginalContent("Original content 2").ComputePrice().Build()
            };

            var queryable = _translationJobs.AsQueryable();
            _mockTranslatonJobSet.As<IQueryable<TranslationJob>>().Setup(m => m.Provider).Returns(queryable.Provider);
            _mockTranslatonJobSet.As<IQueryable<TranslationJob>>().Setup(m => m.Expression).Returns(queryable.Expression);
            _mockTranslatonJobSet.As<IQueryable<TranslationJob>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            _mockTranslatonJobSet.As<IQueryable<TranslationJob>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            _mockContext.Setup(c => c.TranslationJobs).Returns(_mockTranslatonJobSet.Object);

            _translationJobRepository = new TranslationJobRepository(_mockContext.Object);
        }

        [Fact]
        public void GetJobs_ShouldReturnAllJobs()
        {
            var jobs = _translationJobRepository.GetJobs().ToList();

            Assert.Equal(2, jobs.Count);
        }

        [Fact]
        public async Task CreateJobAsync_ShouldAddJob()
        {
            var newJob = _builder.WithCustomerName("Customer 3").WithStatus(JobStatus.New).WithOriginalContent("Original content 3").ComputePrice().Build();

            await _translationJobRepository.CreateJobAsync(newJob, CancellationToken.None);

            _mockTranslatonJobSet.Verify(m => m.Add(It.IsAny<TranslationJob>()), Times.Once);
            _mockContext.Verify(m => m.SaveChangesAsync(CancellationToken.None), Times.Once);
        }
    }

}
