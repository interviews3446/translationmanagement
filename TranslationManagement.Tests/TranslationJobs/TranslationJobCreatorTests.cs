﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Api.Processors.Actions;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Entities.TranslationJob;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Tests.TranslationJobs
{
    public class TranslationJobCreatorTests
    {
        private readonly Mock<IOptions<TranslationManagementOptions>> _mockOptions;
        private readonly Mock<ITranslationJobRepository> _mockRepo;
        private readonly Mock<INotificationServiceHandler> _mockNotificationServiceHandler;
        private readonly Mock<ILogger> _mockLogger;
        private readonly JobCreator _jobCreator;

        public TranslationJobCreatorTests()
        {
            _mockOptions = new Mock<IOptions<TranslationManagementOptions>>{};
            _mockRepo = new Mock<ITranslationJobRepository>();
            _mockNotificationServiceHandler = new Mock<INotificationServiceHandler>();
            _mockLogger = new Mock<ILogger>();

            _jobCreator = new JobCreator(_mockOptions.Object, _mockRepo.Object, _mockNotificationServiceHandler.Object, _mockLogger.Object);

            _mockOptions.SetupGet(o => o.Value).Returns(new TranslationManagementOptions { PricePerCharacter = (decimal)0.01 });
        }

        [Fact]
        public async Task CreateJobAsync_CreatesAndSavesJobSuccessfully()
        {
            var fakeJob = new TranslationJob { Id = 1, TranslatedContent = "content 1" };
            _mockRepo.Setup(r => r.CreateJobAsync(It.IsAny<TranslationJob>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(fakeJob);

            var request = new CreateJobRequest { CustomerName = "", OriginalContent = ""};

            var result = await _jobCreator.CreateJobAsync(request);

            Assert.NotNull(result);
            _mockRepo.Verify(r => r.CreateJobAsync(It.IsAny<TranslationJob>(), It.IsAny<CancellationToken>()), Times.Once);
            _mockNotificationServiceHandler.Verify(n => n.SendNotificationAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once);
        }

    }
}
