﻿using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Data.Entities.TranslationJob
{
    public class AssignedTranslation
    {
        public int TranslationJobId { get; set; }
        public int TranslatorId { get; set; }
        public int PricePerCharacter { get; set; }
        public int TotalCharacters { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime AssignedDate { get; set; }
        public TranslationStatus Status { get; set; }
        public DateTime? CompletedDate { get; set; }
    }
}
