﻿using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Data.Entities.TranslationJob
{
    public class TranslationJob
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public JobStatus Status { get; set; }
        public string OriginalContent { get; set; }
        public string TranslatedContent { get; set; }
        public decimal Price { get; set; }
    }
}
