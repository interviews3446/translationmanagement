﻿namespace TranslationManagement.Data.Entities.Enums
{
    public enum JobStatus
    {
        New = 1,
        InProgress,
        Completed
    }
}
