﻿namespace TranslationManagement.Data.Entities.Enums
{
    public enum TranslatorStatus
    {
        Applicant = 1,
        Certified,
        Deleted
    }
}
