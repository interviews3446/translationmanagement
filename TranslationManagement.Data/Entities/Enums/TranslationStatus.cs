﻿namespace TranslationManagement.Data.Entities.Enums
{
    public enum TranslationStatus
    {
        Pending = 1,
        InProgress,
        Completed,
        Cancelled
    }
}
