﻿using Microsoft.EntityFrameworkCore;
using TranslationManagement.Data.Entities.TranslationJob;
using TranslationManagement.Data.Entities.TranslatorManagement;

namespace TranslationManagement.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {

        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TranslationJob>()
                .Property(x => x.Status)
                .HasConversion<string>();


            modelBuilder.Entity<TranslatorModel>()
                .Property(x => x.Status)
                .HasConversion<string>();

            modelBuilder.Entity<AssignedTranslation>()
                .Property(x => x.Status)
                .HasConversion<string>();

            modelBuilder.Entity<AssignedTranslation>()
            .HasKey(x => new { x.TranslationJobId, x.TranslatorId });

        }

        public virtual DbSet<TranslationJob> TranslationJobs { get; set; } = null!;
        public virtual DbSet<TranslatorModel> Translators { get; set; } = null!;
        public virtual DbSet<AssignedTranslation> AssignedTranslations { get; set; } = null!;
    }
}