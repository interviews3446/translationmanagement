﻿using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Entities.TranslatorManagement;

namespace TranslationManagement.Data.Builders
{
    public class TranslatorModelBuilder
    {
        private TranslatorModel Instance;

        public TranslatorModelBuilder()
        {
            Instance = new TranslatorModel
            {
                Id = 0
            };
        }

        public TranslatorModelBuilder WithName(string name)
        {
            Instance.Name = name;
            return this;
        }

        public TranslatorModelBuilder WithHourlyRate(string hourlyRate)
        {
            Instance.HourlyRate = hourlyRate;
            return this;
        }

        public TranslatorModelBuilder WithStatus(TranslatorStatus status)
        {
            Instance.Status = status;
            return this;
        }

        public TranslatorModelBuilder WithCreditCardNumber(string creditCardNumber)
        {
            Instance.CreditCardNumber = creditCardNumber;
            return this;
        }

        public TranslatorModel Build()
        {
            try
            {
                return Instance;
            }
            finally
            {
                Instance = new TranslatorModel
                {
                    Id = 0
                };
            }
        }
    }
}
