﻿using Microsoft.Extensions.Options;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Entities.TranslationJob;

namespace TranslationManagement.Data.Builders
{
    public class TranslationJobBuilder
    {
        private readonly TranslationManagementOptions _options;
        private TranslationJob Instance { get; set; }

        public TranslationJobBuilder(IOptions<TranslationManagementOptions> options)
        {
            _options = options.Value;
            Instance = new TranslationJob
            {
                Id = 0,
                TranslatedContent = string.Empty
            };
        }

        public TranslationJobBuilder WithCustomerName(string customerName)
        {
            Instance.CustomerName = customerName;
            return this;
        }

        public TranslationJobBuilder WithStatus(JobStatus status)
        {
            Instance.Status = status;
            return this;
        }

        public TranslationJobBuilder WithOriginalContent(string originalContent)
        {
            Instance.OriginalContent = originalContent;
            return this;
        }

        public TranslationJobBuilder ComputePrice()
        {
            Instance.Price = Instance.OriginalContent.Length * _options.PricePerCharacter;
            return this;
        }

        public TranslationJob Build()
        {
            try
            {
                return Instance;

            }
            finally
            {
                Instance = new TranslationJob
                {
                    Id = 0,
                    TranslatedContent = string.Empty

                };
            }
        }
    }
}
