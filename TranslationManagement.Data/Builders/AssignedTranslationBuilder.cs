﻿using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Entities.TranslationJob;

namespace TranslationManagement.Data.Builders
{
    public class AssignedTranslationBuilder
    {
        private AssignedTranslation Instance { get; set; }

        public AssignedTranslationBuilder()
        {
            Instance = new AssignedTranslation()
            {
                Status = TranslationStatus.Pending
            };
        }

        public AssignedTranslationBuilder SetTranslationJobId(int translationJobId)
        {
            Instance.TranslationJobId = translationJobId;
            return this;
        }

        public AssignedTranslationBuilder SetTranslatorId(int translatorId)
        {
            Instance.TranslatorId = translatorId;
            return this;
        }

        public AssignedTranslationBuilder SetHourlyRate(string hourlyRate)
        {
            var pricePerCharacter = SomeMagicMethodThatCalculatesPricePerCharacterFromHourlyRate(hourlyRate);
            Instance.PricePerCharacter = pricePerCharacter;
            return this;
        }

        private int SomeMagicMethodThatCalculatesPricePerCharacterFromHourlyRate(string hourlyRate)
        {
            // HERE CAN BE SOME KIND OF PARSING AND CALCULATIONS
            // BUT FOR THE SAKE OF SIMPLICITY LET'S JUST RETURN RANDOM NUMBER
            var r = new Random();
            return r.Next(1, 100);
        }

        public AssignedTranslationBuilder CalculateTotalPrice()
        {
            Instance.TotalPrice = Instance.TotalCharacters * Instance.PricePerCharacter;
            return this;
        }

        public AssignedTranslationBuilder SetAssignedDate(DateTime assignedDate)
        {
            Instance.AssignedDate = assignedDate;
            return this;
        }

        public AssignedTranslation Build()
        {
            try
            {
                return Instance;
            }
            finally
            {
                Instance = new AssignedTranslation()
                {
                    Status = TranslationStatus.Pending
                };
            }
        }
    }
}
