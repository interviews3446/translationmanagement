﻿using Microsoft.EntityFrameworkCore;
using TranslationManagement.Common.Exceptions;
using TranslationManagement.Data.Entities.TranslationJob;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Data.Repositories
{
    public class TranslationJobRepository : ITranslationJobRepository
    {
        private readonly AppDbContext _context;

        public TranslationJobRepository(AppDbContext context)
        {
            _context = context;
        }

        public IQueryable<TranslationJob> GetJobs()
        {
            return _context.TranslationJobs;
        }

        public async Task<TranslationJob> CreateJobAsync(TranslationJob translationJob, CancellationToken cancellationToken = default)
        {
            _context.TranslationJobs.Add(translationJob);
            await _context.SaveChangesAsync(cancellationToken);

            return translationJob;
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
