﻿using TranslationManagement.Data.Entities.TranslatorManagement;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Data.Repositories
{
    public class TranslationManagementRepository : ITranslationManagementRepository
    {
        private readonly AppDbContext _context;

        public TranslationManagementRepository(AppDbContext context)
        {
            _context = context;
        }

        public IQueryable<TranslatorModel> GetTranslators()
        {
            return _context.Translators;
        }

        public async Task<TranslatorModel> CreateTranslatorAsync(TranslatorModel translator, CancellationToken cancellationToken)
        {
            await _context.Translators.AddAsync(translator, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            return translator;
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
