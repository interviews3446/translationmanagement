﻿using TranslationManagement.Data.Entities.TranslationJob;

namespace TranslationManagement.Data.Repositories.Interfaces;

public interface ITranslationJobRepository
{
    IQueryable<TranslationJob> GetJobs();
    Task<TranslationJob> CreateJobAsync(TranslationJob translationJob, CancellationToken cancellationToken);
    Task SaveChangesAsync(CancellationToken cancellationToken);
}