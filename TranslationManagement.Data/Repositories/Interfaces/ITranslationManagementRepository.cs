﻿using TranslationManagement.Data.Entities.TranslatorManagement;

namespace TranslationManagement.Data.Repositories.Interfaces;

public interface ITranslationManagementRepository
{
    IQueryable<TranslatorModel> GetTranslators();
    Task<TranslatorModel> CreateTranslatorAsync(TranslatorModel translator, CancellationToken cancellationToken);
    Task SaveChangesAsync(CancellationToken cancellationToken);
}