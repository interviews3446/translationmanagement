﻿using TranslationManagement.Data.Entities.TranslationJob;

namespace TranslationManagement.Data.Repositories;

public interface IAssignedTranslationsRepository
{
    IQueryable<AssignedTranslation> GetAssignedTranslations();

    Task<AssignedTranslation> CreateAssignedTranslationAsync(AssignedTranslation assignedTranslation
        , CancellationToken cancellationToken = default);

    Task SaveChangesAsync(CancellationToken cancellationToken = default);
}