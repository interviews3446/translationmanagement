﻿using TranslationManagement.Data.Entities.TranslationJob;

namespace TranslationManagement.Data.Repositories
{
    public class AssignedTranslationsRepository : IAssignedTranslationsRepository
    {
        private readonly AppDbContext _context;

        public AssignedTranslationsRepository(AppDbContext context)
        {
            _context = context;
        }

        public IQueryable<AssignedTranslation> GetAssignedTranslations()
        {
            return _context.AssignedTranslations;
        }

        public async Task<AssignedTranslation> CreateAssignedTranslationAsync(AssignedTranslation assignedTranslation
            , CancellationToken cancellationToken = default)
        {
            await _context.AssignedTranslations.AddAsync(assignedTranslation, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            return assignedTranslation;
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
