using External.ThirdParty.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;
using TranslationManagement.Api.Handlers;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Api.Middlewares;
using TranslationManagement.Api.Processors;
using TranslationManagement.Api.Processors.Interfaces;
using TranslationManagement.Common.Options;
using TranslationManagement.Data;
using TranslationManagement.Data.Repositories;
using TranslationManagement.Data.Repositories.Interfaces;
using ExceptionHandlerMiddleware = TranslationManagement.Api.Middlewares.ExceptionHandlerMiddleware;
using FluentValidation;
using FluentValidation.AspNetCore;

namespace TranslationManagement.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<TranslationManagementOptions>(Configuration.GetSection("AppConfiguration"));
            
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            }); 
            ;
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TranslationManagement.Api", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAngularFrontend",
                    builder => builder.WithOrigins("http://localhost:4200") // Angular app origin
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });

            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlite("Data Source=TranslationAppDatabase.db"));

            services.AddScoped<ITranslationJobRepository, TranslationJobRepository>();
            services.AddScoped<ITranslationJobProcessor, TranslationJobProcessor>();
            services.AddScoped<INotificationServiceHandler, NotificationServiceHandler>();

            services.AddTransient<INotificationService, UnreliableNotificationService>();

            services.AddFluentValidationAutoValidation();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<TranslationManagementOptions> options)
        {
            TranslationManagementOptions.AreAllPropertiesSet(options);

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TranslationManagement.Api v1"));

            app.UseCors("AllowAngularFrontend");
            app.UseRouting();
            app.UseAuthorization();

            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
