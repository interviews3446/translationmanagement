﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Builders;
using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Entities.TranslationJob;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors.Actions
{
    public abstract class AbstractJobCreator
    {
        protected readonly IOptions<TranslationManagementOptions> _options;
        protected readonly ITranslationJobRepository _translationJobRepository;
        protected readonly INotificationServiceHandler _notificationServiceHandler;
        protected readonly ILogger _logger;

        protected AbstractJobCreator(IOptions<TranslationManagementOptions> options,
            ITranslationJobRepository translationJobRepository,
            INotificationServiceHandler notificationServiceHandler,
            ILogger logger)
        {
            _options = options;
            _translationJobRepository = translationJobRepository;
            _notificationServiceHandler = notificationServiceHandler;
            _logger = logger;
        }

        public async Task<TranslationJob> CreateJobAsync(CreateJobRequest request,
            CancellationToken cancellationToken = default)
        {
            var content = await GetContentAsync(request, cancellationToken);

            var jobBuilder = new TranslationJobBuilder(_options);
            var newJob = jobBuilder.WithOriginalContent(content)
                .WithCustomerName(request.CustomerName)
                .WithStatus(JobStatus.New)
                .ComputePrice()
                .Build();

            var createdJob = await SaveCreatedJobToDbAsync(newJob, cancellationToken);
            await _notificationServiceHandler.SendNotificationAsync($"Job created {createdJob.Id}", cancellationToken);

            return createdJob;
        }

        protected virtual async Task<string> GetContentAsync(CreateJobRequest request,
            CancellationToken cancellationToken = default)
        {
            return await Task.FromResult(request.OriginalContent);
        }

        protected async Task<string> GetContentFromFileAsync(CreateJobRequest request,
            CancellationToken cancellationToken = default)
        {
            using var reader = new StreamReader(request.File.OpenReadStream());
            string content;

            try
            {
                content = await reader.ReadToEndAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while reading file: {ex.Message}");
                throw;
            }

            return content;
        }

        private async Task<TranslationJob> SaveCreatedJobToDbAsync(TranslationJob translationJob,
            CancellationToken cancellationToken = default)
        {
            TranslationJob createdJob;
            try
            {
                createdJob = await _translationJobRepository.CreateJobAsync(translationJob, cancellationToken);
                _logger.LogInformation($"Job created: {createdJob.Id}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while creating job: {ex.Message}");
                throw;
            }

            return createdJob;
        }
    }
}
