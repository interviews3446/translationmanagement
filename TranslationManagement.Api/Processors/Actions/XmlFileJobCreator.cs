﻿using System;
using System.IO;
using System.Reflection.Metadata;
using System.Reflection.PortableExecutable;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors.Actions
{
    public class XmlFileJobCreator : AbstractJobCreator
    {
        public XmlFileJobCreator(IOptions<TranslationManagementOptions> options,
            ITranslationJobRepository translationJobRepository, 
            INotificationServiceHandler notificationServiceHandler,
            ILogger logger) : base(options, translationJobRepository, notificationServiceHandler, logger)
        {
        }

        protected override async Task<string> GetContentAsync(CreateJobRequest request, CancellationToken cancellationToken = default)
        {
            var content = await GetContentFromFileAsync(request, cancellationToken);

            try
            {
                var xDocument = XDocument.Parse(content);
                ArgumentNullException.ThrowIfNull(xDocument);
                ArgumentNullException.ThrowIfNull(xDocument.Root);

                content = xDocument.Root.Element("Content")?.Value;
                request.CustomerName = xDocument.Root.Element("Customer")?.Value.Trim();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while parsing xml file: {ex.Message}");
                throw;
            }

            return content;
        }
    }
}
