﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors.Actions
{
    public class TextFileJobCreator : AbstractJobCreator
    {
        public TextFileJobCreator(IOptions<TranslationManagementOptions> options,
            ITranslationJobRepository translationJobRepository,
            INotificationServiceHandler notificationServiceHandler,
            ILogger logger) : base(options, translationJobRepository, notificationServiceHandler, logger)
        {

        }

        protected override async Task<string> GetContentAsync(CreateJobRequest request, CancellationToken cancellationToken = default)
        {
            return await GetContentFromFileAsync(request, cancellationToken);
        }
    }
}
