﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors.Actions
{
    public class JobCreatorFactory
    {
        private readonly IOptions<TranslationManagementOptions> _options;
        private readonly ITranslationJobRepository _translationJobRepository;
        private readonly INotificationServiceHandler _notificationServiceHandler;
        private readonly ILogger _logger;

        public JobCreatorFactory(IOptions<TranslationManagementOptions> options,
            ITranslationJobRepository translationJobRepository,
            INotificationServiceHandler notificationServiceHandler,
            ILogger logger)
        {
            _options = options;
            _translationJobRepository = translationJobRepository;
            _notificationServiceHandler = notificationServiceHandler;
            _logger = logger;
        }

        public AbstractJobCreator CreateJobCreator(IFormFile requestFile = null)
        {
            if (requestFile is null)
            {
                return new JobCreator(_options, _translationJobRepository, _notificationServiceHandler, _logger);
            }

            var lastFourChars = requestFile.FileName.Substring(requestFile.FileName.Length - 4);

            return lastFourChars switch
            {
                ".xml" => new XmlFileJobCreator(_options, _translationJobRepository, _notificationServiceHandler, _logger)
                , ".txt" => new TextFileJobCreator(_options, _translationJobRepository, _notificationServiceHandler, _logger)
                , _ => throw new NotSupportedException($"Unsupported file type {lastFourChars}")
            };
        }
    }
}
