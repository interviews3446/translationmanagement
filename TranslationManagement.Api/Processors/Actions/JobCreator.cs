﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors.Actions
{
    public class JobCreator : AbstractJobCreator
    {
        public JobCreator(IOptions<TranslationManagementOptions> options,
            ITranslationJobRepository translationJobRepository,
            INotificationServiceHandler notificationServiceHandler,
            ILogger logger) : base(options, translationJobRepository, notificationServiceHandler, logger)
        {
        }
    }
}
