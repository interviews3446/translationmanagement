﻿using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.TranslationManagement.CreateTranslator;
using TranslationManagement.Api.DTOs.TranslationManagement.GetAssignedTranslations;
using TranslationManagement.Api.DTOs.TranslationManagement.GetTranslators;
using TranslationManagement.Api.DTOs.TranslationManagement.TranslateJobByTranslator;
using TranslationManagement.Api.DTOs.TranslationManagement.UpdateTranslator;

namespace TranslationManagement.Api.Processors.Interfaces;

public interface ITranslationManagementProcessor
{
    Task<GetTranslatorsResponse> GetTranslatorsAsync(string name, CancellationToken cancellationToken = default);
    Task<CreateTranslatorResponse> CreateTranslatorAsync(CreateTranslatorRequest request, CancellationToken cancellationToken = default);
    Task<UpdateTranslatorResponse> UpdateTranslatorStatusAsync(UpdateTranslatorRequest request, CancellationToken cancellationToken = default);
    Task<AssignJobToTranslatorResponse> AssignJobToTranslatorAsync(AssignJobToTranslatorRequest request, CancellationToken cancellationToken);
    Task<GetAssignedTranslationsResponse> GetAssignedTranslationsAsync(string name, CancellationToken cancellationToken);
}