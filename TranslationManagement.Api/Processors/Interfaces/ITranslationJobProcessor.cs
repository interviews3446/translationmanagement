﻿using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.DTOs.TranslationJob.GetJobs;
using TranslationManagement.Api.DTOs.TranslationJob.UpdateJobStatus;

namespace TranslationManagement.Api.Processors.Interfaces;

public interface ITranslationJobProcessor
{
    Task<GetJobsResponse> GetJobsAsync(CancellationToken cancellationToken = default);
    Task<CreateJobResponse> CreateJobAsync(CreateJobRequest request, CancellationToken cancellationToken = default);
    Task<UpdateJobStatusResponse> UpdateJobStatus(UpdateJobStatusRequest request, CancellationToken cancellationToken = default);
}