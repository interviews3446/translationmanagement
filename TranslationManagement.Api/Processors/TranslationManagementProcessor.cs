﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.Mappers;
using TranslationManagement.Api.DTOs.TranslationManagement.CreateTranslator;
using TranslationManagement.Api.DTOs.TranslationManagement.GetAssignedTranslations;
using TranslationManagement.Api.DTOs.TranslationManagement.GetTranslators;
using TranslationManagement.Api.DTOs.TranslationManagement.TranslateJobByTranslator;
using TranslationManagement.Api.DTOs.TranslationManagement.UpdateTranslator;
using TranslationManagement.Api.Processors.Interfaces;
using TranslationManagement.Common.Exceptions;
using TranslationManagement.Data.Builders;
using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Repositories;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors
{
    public class TranslationManagementProcessor : ITranslationManagementProcessor
    {
        private readonly ITranslationManagementRepository _translationManagementRepository;
        private readonly ILogger<TranslationManagementProcessor> _logger;
        private readonly ITranslationJobRepository _translationJobRepository;
        private readonly IAssignedTranslationsRepository _assignedTranslationsRepository;

        public TranslationManagementProcessor(ITranslationManagementRepository translationManagementRepository,
            ILogger<TranslationManagementProcessor> logger,
            ITranslationJobRepository translationJobRepository,
            IAssignedTranslationsRepository assignedTranslationsRepository)
        {
            _translationManagementRepository = translationManagementRepository;
            _logger = logger;
            _translationJobRepository = translationJobRepository;
            _assignedTranslationsRepository = assignedTranslationsRepository;
        }


        public async Task<GetTranslatorsResponse> GetTranslatorsAsync(string name, CancellationToken cancellationToken = default)
        {
            var translatorsQuery = _translationManagementRepository.GetTranslators();

            if (name != null)
            {
                translatorsQuery = translatorsQuery.Where(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            }

            var translators = await translatorsQuery
                .Select(x => x.ToDTO())
                .ToListAsync(cancellationToken);

            return new GetTranslatorsResponse()
            {
                Translators = translators
            };
        }

        public async Task<CreateTranslatorResponse> CreateTranslatorAsync(CreateTranslatorRequest request, CancellationToken cancellationToken = default)
        {
            var translatorBuilder = new TranslatorModelBuilder();
            var translator = translatorBuilder
                .WithName(request.Name)
                .WithCreditCardNumber(request.CreditCardNumber)
                .WithHourlyRate(request.HourlyRate)
                .Build();

            try
            {
                var createdTranslator = await _translationManagementRepository.CreateTranslatorAsync(translator, cancellationToken);

                return new CreateTranslatorResponse()
                {
                    Translator = createdTranslator.ToDTO()
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error while creating translator: {e.Message}");
                throw;
            }
        }

        public async Task<UpdateTranslatorResponse> UpdateTranslatorStatusAsync(UpdateTranslatorRequest request,
            CancellationToken cancellationToken = default)
        {
            _logger.LogInformation($"Translator update request received: {request.Status} for user {request.Id}");

            var currentTranslator = await _translationManagementRepository
                .GetTranslators()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (currentTranslator == null)
            {
                throw new EntityNotFoundException($"Translator with id {request.Id} does not exists");
            }

            currentTranslator.Status = request.Status;

            try
            {
                await _translationManagementRepository.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while updating translator status: {ex.Message}");
                throw;
            }

            return new UpdateTranslatorResponse()
            {
                Translator = currentTranslator.ToDTO()
            };
        }

        public async Task<AssignJobToTranslatorResponse> AssignJobToTranslatorAsync(AssignJobToTranslatorRequest request
            , CancellationToken cancellationToken = default)
        {
            var currentTranslator = await _translationManagementRepository
                .GetTranslators()
                .FirstOrDefaultAsync(x => x.Id == request.TranslatorId, cancellationToken);

            if (currentTranslator is null)
            {
                throw new EntityNotFoundException($"Translator with id {request.TranslatorId} does not exists");
            }

            if (currentTranslator.Status != TranslatorStatus.Certified)
            {
                throw new BusinessRuleException($"Translator with id {request.TranslatorId} is not Certified");
            }

            var currentJob = await _translationJobRepository
                .GetJobs()
                .FirstOrDefaultAsync(x => x.Id == request.JobId, cancellationToken);

            if (currentJob is null)
            {
                throw new EntityNotFoundException($"Job with id {request.JobId} does not exists");
            }

            if (currentJob.Status != JobStatus.New)
            {
                throw new BusinessRuleException($"Job with id {request.JobId} is not in New status");
            }

            var builder = new AssignedTranslationBuilder();
            var assignedTranslation = builder
                .SetAssignedDate(DateTime.Now)
                .SetTranslationJobId(currentJob.Id)
                .SetTranslatorId(currentTranslator.Id)
                .SetHourlyRate(currentTranslator.HourlyRate)
                .CalculateTotalPrice()
                .Build();

            await _assignedTranslationsRepository.CreateAssignedTranslationAsync(assignedTranslation, cancellationToken);

            currentJob.Status = JobStatus.InProgress;
            await _translationJobRepository.SaveChangesAsync(cancellationToken);
            return new AssignJobToTranslatorResponse
            {
                TotalPrice = assignedTranslation.TotalPrice,
                Message = $"Job {currentJob.Id} has been assigned to {currentTranslator.Name} for total price of {assignedTranslation.TotalPrice}"
            };
        }

        public async Task<GetAssignedTranslationsResponse> GetAssignedTranslationsAsync(string name, CancellationToken cancellationToken)
        {
            var translator  = await _translationManagementRepository
                .GetTranslators()
                .FirstOrDefaultAsync(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase), cancellationToken);

            if (translator is null)
            {
                throw new EntityNotFoundException($"Translator with name {name} does not exists");
            }

            var assignedTranslations = await _assignedTranslationsRepository
                .GetAssignedTranslations()
                .Where(x => x.TranslatorId == translator.Id)
                .ToListAsync(cancellationToken);

            var response = new GetAssignedTranslationsResponse
            {
                AssignedTranslations = assignedTranslations.Select(x => new GetAssignedTranslationsResponseItem()
                {
                    Status =x.Status,
                    TotalPrice = x.TotalPrice,
                    JobId = x.TranslationJobId,
                    TranslatorName = name
                }).ToList()
            };

            return response;
        }
    }
}
