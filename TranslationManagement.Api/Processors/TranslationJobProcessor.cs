﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.Mappers;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.DTOs.TranslationJob.GetJobs;
using TranslationManagement.Api.DTOs.TranslationJob.UpdateJobStatus;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Api.Processors.Actions;
using TranslationManagement.Api.Processors.Interfaces;
using TranslationManagement.Common.Exceptions;
using TranslationManagement.Common.Options;
using TranslationManagement.Data.Entities.Enums;
using TranslationManagement.Data.Repositories.Interfaces;

namespace TranslationManagement.Api.Processors
{
    public class TranslationJobProcessor : ITranslationJobProcessor
    {
        private readonly ITranslationJobRepository _translationJobRepository;
        private readonly ILogger<TranslationJobProcessor> _logger;
        private readonly INotificationServiceHandler _notificationServiceHandler;
        private readonly IOptions<TranslationManagementOptions> _options;

        public TranslationJobProcessor(ITranslationJobRepository translationJobRepository,
            ILogger<TranslationJobProcessor> logger,
            INotificationServiceHandler notificationServiceHandler,
            IOptions<TranslationManagementOptions> options)
        {
            _translationJobRepository = translationJobRepository;
            _logger = logger;
            _notificationServiceHandler = notificationServiceHandler;
            _options = options;
        }

        public async Task<GetJobsResponse> GetJobsAsync(CancellationToken cancellationToken = default)
        {
            var jobs = await _translationJobRepository.GetJobs().ToListAsync(cancellationToken);
            return new GetJobsResponse
            {
                Jobs = jobs.Select(x => x.ToDTO()).ToList()
            };
        }

        public async Task<CreateJobResponse> CreateJobAsync(CreateJobRequest request, CancellationToken cancellationToken = default)
        {
            var createJobFactory = new JobCreatorFactory(_options, _translationJobRepository, _notificationServiceHandler, _logger);
            var jobCreator = createJobFactory.CreateJobCreator(request.File);

            var createdJob = await jobCreator.CreateJobAsync(request, cancellationToken);

            return new CreateJobResponse()
            {
                Job = createdJob.ToDTO()
            };
        }

        public async Task<UpdateJobStatusResponse> UpdateJobStatus(UpdateJobStatusRequest request, CancellationToken cancellationToken = default)
        {
            _logger.LogInformation($"Job status update request received: {request.Status} for job {request.Id} by translator {request.TranslatorId}");

            var currentJob = await _translationJobRepository
                .GetJobs()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (currentJob == null)
            {
                throw new EntityNotFoundException($"TranslationJob with id {request.Id} does not exists");
            }

            var isInvalidStatusChange = (currentJob.Status == JobStatus.New && request.Status == JobStatus.Completed) ||
                                         currentJob.Status == JobStatus.Completed ||
                                         request.Status == JobStatus.New;

            if (isInvalidStatusChange)
            {
                throw new BusinessRuleException(
                    $"Error - TranslationJob status update failed. Cant update {currentJob.Status} to {request.Status}");
            }

            currentJob.Status = request.Status;

            try
            {
                await _translationJobRepository.SaveChangesAsync(cancellationToken);

                return new UpdateJobStatusResponse()
                {
                    Job = currentJob.ToDTO()
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while updating job status: {ex.Message}");
                throw;
            }
        }
    }
}
