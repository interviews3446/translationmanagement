﻿using FluentValidation;
using TranslationManagement.Api.DTOs.TranslationJob.UpdateJobStatus;

namespace TranslationManagement.Api.Validators.TranslationJob
{
    public class UpdateJobStatusValidator : AbstractValidator<UpdateJobStatusRequest>
    {
        public UpdateJobStatusValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0);

            RuleFor(x => x.TranslatorId)
                .GreaterThan(0);

            RuleFor(x => x.Status)
                .IsInEnum()
                .WithMessage("Invalid status change");
        }
    }
}
