﻿using FluentValidation;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;

namespace TranslationManagement.Api.Validators.TranslationJob
{
    public class CreateJobValidator: AbstractValidator<CreateJobRequest>
    {
        public CreateJobValidator()
        {
            RuleFor(x => x.CustomerName)
                .NotEmpty();

            RuleFor(x => x.OriginalContent)
                .NotEmpty();

        }
    }
}
