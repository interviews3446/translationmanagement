﻿using System.Threading;
using FluentValidation;
using TranslationManagement.Api.DTOs.TranslationManagement.CreateTranslator;

namespace TranslationManagement.Api.Validators.TranslatorManagement
{
    public class CreateTranslatorValidator: AbstractValidator<CreateTranslatorRequest>
    {
        public CreateTranslatorValidator()
        {
            RuleFor(x => x.CreditCardNumber).CreditCard();
            RuleFor(x => x.HourlyRate).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
