﻿using FluentValidation;
using TranslationManagement.Api.DTOs.TranslationManagement.UpdateTranslator;

namespace TranslationManagement.Api.Validators.TranslatorManagement
{
    public class UpdateTranslatorValidator : AbstractValidator<UpdateTranslatorRequest>
    {
        public UpdateTranslatorValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0);

            RuleFor(x => x.Status)
                .IsInEnum();
        }
    }
}
