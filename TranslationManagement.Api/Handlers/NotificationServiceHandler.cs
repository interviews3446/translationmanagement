﻿using System;
using System.Threading;
using System.Threading.Tasks;
using External.ThirdParty.Services;
using Microsoft.Extensions.Logging;
using TranslationManagement.Api.Handlers.Interfaces;
using TranslationManagement.Common.Exceptions;

namespace TranslationManagement.Api.Handlers
{
    public class NotificationServiceHandler : INotificationServiceHandler
    {
        private readonly INotificationService _notificationService;
        private readonly ILogger<NotificationServiceHandler> _logger;

        public NotificationServiceHandler(INotificationService notificationService, ILogger<NotificationServiceHandler> logger)
        {
            _notificationService = notificationService;
            _logger = logger;
        }

        public async Task SendNotificationAsync(string message, CancellationToken cancellationToken = default)
        {
            var maxRetries = 10;
            var currentAttempt = 0;

            while (currentAttempt < maxRetries)
            {
                try
                {
                    var notificationSent = await _notificationService.SendNotification(message);

                    if (notificationSent)
                    {
                        break;
                    }

                    _logger.LogWarning($"Warning - Notification was not send. Current attempt: {currentAttempt}");
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, $"Error while sending notification. {ex.Message}.");
                }

                currentAttempt++;
            }

            if (currentAttempt >= maxRetries)
            {
                _logger.LogError($"Error - Notification was not send. Max retries reached.");
                throw new BusinessRuleException("Error - Notification was not send. Max retries reached.");
            }

            _logger.LogInformation("New job notification sent");
        }
    }
}
