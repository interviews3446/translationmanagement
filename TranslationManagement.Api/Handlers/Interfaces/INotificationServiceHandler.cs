﻿using System.Threading;
using System.Threading.Tasks;

namespace TranslationManagement.Api.Handlers.Interfaces;

public interface INotificationServiceHandler
{
    Task SendNotificationAsync(string message, CancellationToken cancellationToken = default);
}