﻿using TranslationManagement.Api.DTOs.TranslationJob;
using TranslationManagement.Api.DTOs.TranslationManagement;

namespace TranslationManagement.Api.DTOs.Mappers
{
    public static class DtoToDboMapper
    {
        public static Data.Entities.TranslationJob.TranslationJob ToDBO(this TranslationJobDTO dto)
        {
            return new Data.Entities.TranslationJob.TranslationJob
            {
                CustomerName = dto.CustomerName,
                OriginalContent = dto.OriginalContent,
                TranslatedContent = dto.TranslatedContent,
                Price = dto.Price,
                Id = dto.Id,
                Status = dto.Status
            };
        }

        public static TranslationJobDTO ToDTO(this Data.Entities.TranslationJob.TranslationJob dbo)
        {
            return new TranslationJobDTO
            {
                CustomerName = dbo.CustomerName,
                OriginalContent = dbo.OriginalContent,
                TranslatedContent = dbo.TranslatedContent,
                Price = dbo.Price,
                Id = dbo.Id,
                Status = dbo.Status
            };
        }

        public static TranslationModelDTO ToDTO(this Data.Entities.TranslatorManagement.TranslatorModel dbo)
        {
            return new TranslationModelDTO
            {
                Id = dbo.Id,
                Name = dbo.Name,
                HourlyRate = dbo.HourlyRate,
                Status = dbo.Status,
                CreditCardNumber = dbo.CreditCardNumber
            };
        }

        public static Data.Entities.TranslatorManagement.TranslatorModel ToDBO(this TranslationModelDTO dto)
        {
            return new Data.Entities.TranslatorManagement.TranslatorModel
            {
                Id = dto.Id,
                Name = dto.Name,
                HourlyRate = dto.HourlyRate,
                Status = dto.Status,
                CreditCardNumber = dto.CreditCardNumber
            };
        }
    }
}
