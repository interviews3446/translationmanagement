﻿namespace TranslationManagement.Api.DTOs.TranslationManagement.TranslateJobByTranslator
{
    public class AssignJobToTranslatorRequest
    {
        public int TranslatorId { get; set; }
        public int JobId { get; set; }
    }
}
