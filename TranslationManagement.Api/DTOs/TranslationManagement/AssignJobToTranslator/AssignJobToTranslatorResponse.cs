﻿namespace TranslationManagement.Api.DTOs.TranslationManagement.TranslateJobByTranslator
{
    public class AssignJobToTranslatorResponse
    {
        public decimal TotalPrice { get; set; }
        public string Message { get; set; }
    }
}
