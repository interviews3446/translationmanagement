﻿using System.Collections.Generic;
using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Api.DTOs.TranslationManagement.GetAssignedTranslations
{
    public class GetAssignedTranslationsResponse
    {
        public IList<GetAssignedTranslationsResponseItem> AssignedTranslations { get; set; }
    }

    public class GetAssignedTranslationsResponseItem
    {
        public int JobId { get; set; }
        public string TranslatorName { get; set; }
        public decimal TotalPrice { get; set; }
        public TranslationStatus Status { get; set; }
    }
}
