﻿namespace TranslationManagement.Api.DTOs.TranslationManagement.CreateTranslator
{
    public class CreateTranslatorRequest
    {
        public string Name { get; set; }
        public string HourlyRate { get; set; }
        public string CreditCardNumber { get; set; }
    }
}
