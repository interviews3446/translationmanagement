﻿namespace TranslationManagement.Api.DTOs.TranslationManagement.CreateTranslator
{
    public class CreateTranslatorResponse
    {
        public TranslationModelDTO Translator { get; set; }
    }
}
