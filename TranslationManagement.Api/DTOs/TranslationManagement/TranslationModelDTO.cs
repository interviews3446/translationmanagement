﻿using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Api.DTOs.TranslationManagement
{
    public class TranslationModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HourlyRate { get; set; }
        public TranslatorStatus Status { get; set; }
        public string CreditCardNumber { get; set; }
    }
}
