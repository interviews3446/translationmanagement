﻿using System.Collections.Generic;

namespace TranslationManagement.Api.DTOs.TranslationManagement.GetTranslators
{
    public class GetTranslatorsResponse
    {
        public IList<TranslationModelDTO> Translators { get; set; }
    }
}
