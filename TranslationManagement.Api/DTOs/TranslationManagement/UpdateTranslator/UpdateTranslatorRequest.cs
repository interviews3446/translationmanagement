﻿using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Api.DTOs.TranslationManagement.UpdateTranslator
{
    public class UpdateTranslatorRequest
    {
        public int Id { get; set; }
        public TranslatorStatus Status { get; set; }
    }
}
