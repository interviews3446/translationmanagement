﻿namespace TranslationManagement.Api.DTOs.TranslationManagement.UpdateTranslator
{
    public class UpdateTranslatorResponse
    {
        public TranslationModelDTO Translator { get; set; }
    }
}
