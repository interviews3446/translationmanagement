﻿using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Api.DTOs.TranslationJob.UpdateJobStatus
{
    public class UpdateJobStatusRequest
    {
        public int Id { get; set; }
        public int TranslatorId { get; set; }
        public JobStatus Status { get; set; }
    }
}
