﻿namespace TranslationManagement.Api.DTOs.TranslationJob.UpdateJobStatus
{
    public class UpdateJobStatusResponse
    {
        public TranslationJobDTO Job { get; set; }
    }
}
