﻿using Microsoft.AspNetCore.Http;

namespace TranslationManagement.Api.DTOs.TranslationJob.CreateJob
{
    public class CreateJobRequest
    {
        public string CustomerName { get; set; }
        public string OriginalContent { get; set; }

        public IFormFile File { get; set; }
    }
}
