﻿namespace TranslationManagement.Api.DTOs.TranslationJob.CreateJob
{
    public class CreateJobResponse
    {
        public TranslationJobDTO Job { get; set; }
    }
}
