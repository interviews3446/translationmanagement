﻿using TranslationManagement.Data.Entities.Enums;

namespace TranslationManagement.Api.DTOs.TranslationJob
{
    public class TranslationJobDTO
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public JobStatus Status { get; set; }
        public string OriginalContent { get; set; }
        public string TranslatedContent { get; set; }
        public decimal Price { get; set; }
    }
}
