﻿using System.Collections.Generic;

namespace TranslationManagement.Api.DTOs.TranslationJob.GetJobs
{
    public class GetJobsResponse
    {
        public IList<TranslationJobDTO> Jobs { get; set; }
    }
}
