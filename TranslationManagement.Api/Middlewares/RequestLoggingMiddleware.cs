﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TranslationManagement.Api.Middlewares
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestLoggingMiddleware> _logger;

        public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            context.Request.EnableBuffering();

            var originalBodyStream = context.Response.Body;

            try
            {
                using var requestBodyStream = new MemoryStream();
                await context.Request.Body.CopyToAsync(requestBodyStream);
                requestBodyStream.Seek(0, SeekOrigin.Begin);

                using var requestBodyText = new StreamReader(requestBodyStream).ReadToEndAsync();
                requestBodyStream.Seek(0, SeekOrigin.Begin);

                var logMessage = new StringBuilder();
                logMessage.AppendLine("HTTP Request Information:");
                logMessage.AppendLine($"Schema: {context.Request.Scheme}");
                logMessage.AppendLine($"Host: {context.Request.Host}");
                logMessage.AppendLine($"Path: {context.Request.Path}");
                logMessage.AppendLine($"QueryString: {context.Request.QueryString}");
                logMessage.AppendLine($"Request Body: {requestBodyText}");

                _logger.LogInformation(logMessage.ToString());

                context.Request.Body = requestBodyStream;

                await _next(context);
            }
            finally
            {
                context.Request.Body = originalBodyStream;
            }
        }
    }
}
