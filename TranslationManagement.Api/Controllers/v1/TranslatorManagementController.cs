﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.TranslationManagement.CreateTranslator;
using TranslationManagement.Api.DTOs.TranslationManagement.GetAssignedTranslations;
using TranslationManagement.Api.DTOs.TranslationManagement.GetTranslators;
using TranslationManagement.Api.DTOs.TranslationManagement.TranslateJobByTranslator;
using TranslationManagement.Api.DTOs.TranslationManagement.UpdateTranslator;
using TranslationManagement.Api.Processors.Interfaces;
using TranslationManagement.Common.Exceptions;

namespace TranslationManagement.Api.Controllers.v1
{
    [ApiController]
    [Route("api/v1/TranslatorsManagement")]
    public class TranslatorManagementController : ControllerBase
    {
        private readonly ITranslationManagementProcessor _translationManagementProcessor;

        public TranslatorManagementController(ITranslationManagementProcessor translationManagementProcessor)
        {
            _translationManagementProcessor = translationManagementProcessor;
        }

        [HttpGet]
        [ProducesResponseType(typeof(GetTranslatorsResponse), (int)HttpStatusCode.OK)]

        public async Task<GetTranslatorsResponse> GetTranslators([FromQuery] string name, CancellationToken cancellationToken = default)
        {
            return await _translationManagementProcessor.GetTranslatorsAsync(name, cancellationToken);
        }

        [HttpPost]
        [ProducesResponseType(typeof(GetTranslatorsResponse), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateTranslator(CreateTranslatorRequest translator, CancellationToken cancellationToken = default)
        {
            var response = await _translationManagementProcessor.CreateTranslatorAsync(translator, cancellationToken);
            return new CreatedResult(nameof(CreateTranslator), response);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(GetTranslatorsResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(BusinessRuleException), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(EntityNotFoundException), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateTranslatorStatus(UpdateTranslatorRequest request, CancellationToken cancellationToken = default)
        {
            var response = await _translationManagementProcessor.UpdateTranslatorStatusAsync(request, cancellationToken);
            return new OkObjectResult(response);
        }

        [HttpPost("assign-translator")]
        [ProducesResponseType(typeof(AssignJobToTranslatorResponse), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> AssignJobToTranslator(AssignJobToTranslatorRequest request, CancellationToken cancellationToken = default)
        {
            var response = await _translationManagementProcessor.AssignJobToTranslatorAsync(request, cancellationToken);
            return new CreatedResult(nameof(AssignJobToTranslator), response);
        }

        [HttpGet("assigned-translations")]
        [ProducesResponseType(typeof(GetAssignedTranslationsResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAssignedTranslations([FromQuery] string name, CancellationToken cancellationToken = default)
        {
            var response = await _translationManagementProcessor.GetAssignedTranslationsAsync(name, cancellationToken);
            return new OkObjectResult(response);
        }
    }
}