﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using TranslationManagement.Api.DTOs.TranslationJob.CreateJob;
using TranslationManagement.Api.DTOs.TranslationJob.GetJobs;
using TranslationManagement.Api.DTOs.TranslationJob.UpdateJobStatus;
using TranslationManagement.Api.Processors.Interfaces;
using TranslationManagement.Common.Exceptions;

namespace TranslationManagement.Api.Controllers.v1
{
    [ApiController]
    [Route("api/v1/jobs")]
    public class TranslationJobController : ControllerBase
    {
        private readonly ITranslationJobProcessor _translationJobProcessor;

        public TranslationJobController(ITranslationJobProcessor translationJobProcessor)
        {
            _translationJobProcessor = translationJobProcessor;
        }

        [HttpGet]
        [ProducesResponseType(typeof(GetJobsResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetJobs(CancellationToken cancellationToken = default)
        {
            var response = await _translationJobProcessor.GetJobsAsync(cancellationToken);
            return new OkObjectResult(response);
        }


        [HttpPost]
        [ProducesResponseType(typeof(CreateJobResponse), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateJob([FromForm] CreateJobRequest job, CancellationToken cancellationToken = default)
        {
            var response = await _translationJobProcessor.CreateJobAsync(job, cancellationToken);
            return new CreatedResult(nameof(CreateJob), response);
        }

        [HttpPatch("status")]
        [ProducesResponseType(typeof(UpdateJobStatusResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(BusinessRuleException), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(EntityNotFoundException), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateJobStatus(UpdateJobStatusRequest request, CancellationToken cancellationToken = default)
        {
            var response = await _translationJobProcessor.UpdateJobStatus(request, cancellationToken);
            return new OkObjectResult(response);
        }
    }
}