﻿namespace TranslationManagement.Common.Options
{
    public class TranslationManagementOptions
    {
        public decimal PricePerCharacter { get; set; }

        public static bool AreAllPropertiesSet<T>(T options) where T : class
        {
            return typeof(T)
                .GetProperties()
                .All(propertyInfo => propertyInfo.GetValue(options) != null);
        }
    }
}
