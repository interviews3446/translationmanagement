import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { TranslatorsPageComponent } from './pages/translators-page/translators-page.component';
import { AssignationPageComponent } from './pages/assignation-page/assignation-page.component';
import { TranslationJobsPageComponent } from './pages/translation-jobs-page/translation-jobs-page.component';

const routes: Routes = [
  { path: 'translationJobs', component: TranslationJobsPageComponent},
  { path: 'translators', component: TranslatorsPageComponent },
  { path: 'assigning', component: AssignationPageComponent },
  { path: '', redirectTo: '/translationJobs', pathMatch: 'full' },
  { path: '**', component: AppComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
