import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TranslationJobGetJobsResponseDTO } from '../models/translation-job-get-jobs-response.dto.model';

@Injectable({
  providedIn: 'root'
})
export class TranslationJobService {
  private apiUrl = 'http://localhost:7729/api/v1/';

  constructor(private http: HttpClient) {}

  getJobs(): Observable<TranslationJobGetJobsResponseDTO> {
    let url = this.apiUrl + 'jobs';
    return this.http.get<TranslationJobGetJobsResponseDTO>(url);
  }

  createJob(jobData: FormData) {
    let url = this.apiUrl + 'jobs';
    return this.http.post(url, jobData);
  }
}