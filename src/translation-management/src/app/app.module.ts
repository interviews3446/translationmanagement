import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { TranslatorsPageComponent } from './pages/translators-page/translators-page.component';
import { AssignationPageComponent } from './pages/assignation-page/assignation-page.component';
import { TranslationJobsPageComponent } from './pages/translation-jobs-page/translation-jobs-page.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslationJobsListComponent } from './pages/translation-jobs-page/translation-jobs-list/translation-jobs-list.component';
import { TranslationJobsAddFormComponent } from './pages/translation-jobs-page/translation-jobs-add-form/translation-jobs-add-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TranslatorsPageComponent,
    AssignationPageComponent,
    TranslationJobsPageComponent,
    TranslationJobsListComponent,
    TranslationJobsAddFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
