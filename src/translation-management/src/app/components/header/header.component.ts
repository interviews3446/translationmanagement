import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent {
  menuItems = [
    {
      name: 'TranslationJobs',
      link: '/',
      isActive: true
    },
    {
      name: 'Translators',
      link: '/translators',
      isActive: false
    },
    {
      name: 'AssignTranslator',
      link: '/assigning',
      isActive: false
    }
  ];

  SetItemActive(name: string): void 
  {
    this.menuItems.forEach(item => {
      item.isActive = item.name === name;
    });
  };
}
