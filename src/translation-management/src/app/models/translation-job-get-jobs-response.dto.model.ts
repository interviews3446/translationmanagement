import { TranslationJobDTO } from "./translation-job-dto.model";

export interface TranslationJobGetJobsResponseDTO {
    jobs: TranslationJobDTO[];
  }