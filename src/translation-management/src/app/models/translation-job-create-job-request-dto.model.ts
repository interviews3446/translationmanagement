export interface TranslationJobCreateJobRequestDTO {
    customerName: string;
    originalContent: string;
    file: File;
  }