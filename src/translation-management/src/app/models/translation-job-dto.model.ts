import { JobStatus } from "./job-status.enum";

export interface TranslationJobDTO {
    id: number;
    customerName: string;
    status: JobStatus; 
    originalContent: string;
    translatedContent: string;
    price: number;
  }