export enum JobStatus {
    New = "New",
    InProgress = "InProgress",
    Completed = "Completed",
}