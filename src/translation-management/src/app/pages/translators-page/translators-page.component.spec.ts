import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslatorsPageComponent } from './translators-page.component';

describe('TranslatorsPageComponent', () => {
  let component: TranslatorsPageComponent;
  let fixture: ComponentFixture<TranslatorsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TranslatorsPageComponent]
    });
    fixture = TestBed.createComponent(TranslatorsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
