import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationJobsAddFormComponent } from './translation-jobs-add-form.component';

describe('TranslationJobsAddFormComponent', () => {
  let component: TranslationJobsAddFormComponent;
  let fixture: ComponentFixture<TranslationJobsAddFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TranslationJobsAddFormComponent]
    });
    fixture = TestBed.createComponent(TranslationJobsAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
