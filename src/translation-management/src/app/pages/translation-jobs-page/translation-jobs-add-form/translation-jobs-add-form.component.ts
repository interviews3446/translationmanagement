import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { JobStatus } from 'src/app/models/job-status.enum';
import { TranslationJobService } from 'src/app/services/translation-job.service';

@Component({
  selector: 'app-translation-jobs-add-form',
  templateUrl: './translation-jobs-add-form.component.html',
  styleUrls: ['./translation-jobs-add-form.component.css']
})


export class TranslationJobsAddFormComponent  {
constructor(private translationJobService: TranslationJobService) {}

  jobStatuses = Object.values(JobStatus);

  jobForm = new FormGroup({
    customerName: new FormControl(''),
    originalContent: new FormControl('')
  });

  selectedFile: File | null = null;
  onFileSelected(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList | null = element.files;
    if (fileList) {
      this.selectedFile = fileList[0];
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('customerName', this.jobForm.value.customerName || '');
    formData.append('originalContent', this.jobForm.value.originalContent || '');

    if (this.selectedFile) {
      formData.append('file', this.selectedFile, this.selectedFile.name);
    }

    this.translationJobService.createJob(formData).subscribe(x=>console.log(x)   
    )
  }
}