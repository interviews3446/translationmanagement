import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-translation-jobs-page',
  templateUrl: './translation-jobs-page.component.html',
  styleUrls: ['./translation-jobs-page.component.css']
})

export class TranslationJobsPageComponent {
  activeTab: string = 'list';

  setActiveTab(tabName: string): void {
    this.activeTab = tabName;    
  }
}
