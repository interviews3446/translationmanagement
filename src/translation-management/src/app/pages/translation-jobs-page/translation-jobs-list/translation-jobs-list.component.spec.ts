import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationJobsListComponent } from './translation-jobs-list.component';

describe('TranslationJobsListComponent', () => {
  let component: TranslationJobsListComponent;
  let fixture: ComponentFixture<TranslationJobsListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TranslationJobsListComponent]
    });
    fixture = TestBed.createComponent(TranslationJobsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
