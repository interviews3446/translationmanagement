import {  Component, OnInit, Input, SimpleChanges, OnChanges} from '@angular/core';
import { TranslationJobDTO } from 'src/app/models/translation-job-dto.model';
import { TranslationJobService } from 'src/app/services/translation-job.service';

@Component({
  selector: 'app-translation-jobs-list',
  templateUrl: './translation-jobs-list.component.html',
  styleUrls: ['./translation-jobs-list.component.css']
})
export class TranslationJobsListComponent implements OnInit, OnChanges {
  jobs: TranslationJobDTO[] = [];
  @Input() activeTab: string = 'list';
  constructor(private translationJobService: TranslationJobService) {}

  ngOnInit() {
    this.fetchJobs();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['activeTab'] && this.activeTab === 'list') {
      this.fetchJobs();
    }
  }

  private fetchJobs() {
    this.translationJobService.getJobs().subscribe(jobs => {
      this.jobs = jobs.jobs;
      console.log(jobs);
    });
  }
}
