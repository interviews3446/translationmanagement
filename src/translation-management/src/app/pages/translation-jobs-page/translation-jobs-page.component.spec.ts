import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationJobsPageComponent } from './translation-jobs-page.component';

describe('TranslationJobsPageComponent', () => {
  let component: TranslationJobsPageComponent;
  let fixture: ComponentFixture<TranslationJobsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TranslationJobsPageComponent]
    });
    fixture = TestBed.createComponent(TranslationJobsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
