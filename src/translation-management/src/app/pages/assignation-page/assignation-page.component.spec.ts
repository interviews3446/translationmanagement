import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignationPageComponent } from './assignation-page.component';

describe('AssignationPageComponent', () => {
  let component: AssignationPageComponent;
  let fixture: ComponentFixture<AssignationPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AssignationPageComponent]
    });
    fixture = TestBed.createComponent(AssignationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
